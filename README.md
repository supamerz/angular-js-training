![AngularJS_logo.svg.png](https://bitbucket.org/repo/A7KyoA/images/2156773004-AngularJS_logo.svg.png)
# AngularJS Class @ WCTC
***

If you're behind a Corporate Proxy, use the below Bash script to set each terminal session to use the Proxy Settings:

#### Setup Proxy Setter via Bash

In your root directory, create a **.bash_profile** if you don't already have it and place the below script inside:

```terminal
#!/bin/bash
set_http_proxy() {
  read -p "User Name: " userName
  read -s -p "Password: " password
  export http_proxy="http://${userName}:${password}@example.com:8888/"
  export https_proxy="http://${userName}:${password}@example.com:8888/"
  echo; echo 'http_proxy variable set for this terminal session!'
}
```
To grab these changes, type:
```terminal
$ source .bash_profile
```

Now type:
```terminal
$ set_http_proxy
```
And type in your User Name and Password upon the prompts. You are now proxied for this terminal session.

**ENSURE PROXY IS SET EACH TIME YOU OPEN A NEW TERMINAL WINDOW**
***


Upon cloning, please run npm install in the angular-js-training directory
```terminal
$ npm install
```

***
## Launching the Environment (Mac)
From a command prompt navigate to the project folder, i.e., `~/angular-js-training`

From there change directories to **backend**, i.e., `cd backend`

Type:

```shell
$ ls -l set-path.sh
-- verify executable bit is set, if not, chmod +x set-path.sh
$ source set-path.sh
```
Then:

```shell
$ db-start
$ db-load
$ start-web-services
$ start-web-server
```
You should be able to navigate to: http://localhost:9080 and see the content for the course
